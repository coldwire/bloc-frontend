# bloc-frontend

frontend server for Bloc

## Requirements

- NodeJS >= 16
- npm or yarn

## Quickstart

```sh
(bloc) $ yarn install
(bloc) $ yarn run dev
```
