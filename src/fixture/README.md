# Fixture Directory

## EN

This directory contain JSON files that could help to make data simulation in
application. He will often be used to be loaded in storage system (**Vuex**, **Pinia**) 
or by units testing.

## FR

Ce repertoire contient des fichiers **JSON** permettant d'effectuer des **simulations de
données** dans l'application. Il sera généralement souvent utilisé pour être chargé
dans les **magasin de stockage (Vuex, Pinia)** ou pour les tests unitaires.
