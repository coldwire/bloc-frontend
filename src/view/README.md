# View Directory

## EN

This directory contain the *"Single Page"* files of application. Organization and 
tree was defined in **router** directory. Each file represent an URI.

## FR

Contient les *"Single Page"*  de l'application. Leurs organisation et arborecence est
définit dans le repertoire **router**.
