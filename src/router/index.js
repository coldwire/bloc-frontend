import { createRouter, createWebHistory } from 'vue-router'

// App Index
import ViewIndex from '@/view/ViewIndex.vue'
import ViewAppIndex from '@/view/app/ViewAppIndex.vue'

// Authentication
import ViewAuthIndex from '@/view/auth/ViewAuthIndex.vue'
import ViewAuthRegister from '@/view/auth/ViewAuthRegister.vue'
import ViewAuthLogin from '@/view/auth/ViewAuthLogin.vue'

// Category
import ViewCategoryIndex from '@/view/app/category/ViewCategoryIndex.vue'
import ViewCategoryFile from '@/view/app/category/ViewCategoryFile.vue'
import ViewCategoryStarred from '@/view/app/category/ViewCategoryStarred.vue'
import ViewCategoryShared from '@/view/app/category/ViewCategoryShared.vue'

// Setting
// import ViewSettingIndex from '@/view/app/setting/ViewSettingIndex.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      component: ViewIndex
    },
    {
      path: '/app/',
      name: 'app',
      component: ViewAppIndex,
      children: [
        {
          path: 'category/',
          name: 'app.category',
          component: ViewCategoryIndex,
          children: [
            {
              path: 'file/',
              name: 'category.file',
              component: ViewCategoryFile
            },
            {
              path: 'starred/',
              name: 'category.starred',
              component: ViewCategoryStarred
            },
            {
              path: 'shared/',
              name: 'category.shared',
              component: ViewCategoryShared
            }
          ]
        }
      ]
    },
    {
      path: '/auth/',
      name: 'auth',
      component: ViewAuthIndex,
      children: [
        {
          path: 'register/',
          name: 'auth.register',
          component: ViewAuthRegister
        },
        {
          path: 'login/',
          name: 'auth.login',
          component: ViewAuthLogin
        }
      ]
    }
  ]
})

export default router
