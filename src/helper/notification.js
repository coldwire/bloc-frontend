class AppNotificationBuilder {
  constructor(title) {
    this.id = Math.floor(Math.random() * 1000000000)
    this.title = title
  }

  setDuration(duration) {
    this.duration = duration
    return this
  }

  setIsClosable(isClosable: true) {
    this.isClosable = isClosable
    return this
  }

  build() {
    return new AppNotificationBuilder(
      this.id,
      this.title,
      this.isClosable,
      this.duration
    )
  }
}


