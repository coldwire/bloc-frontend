// TODO: Make mimetype conversion on backend and return specific 'Bloc' type. hashmap for example with { "video/mp4": "video" } in server side
//--------------------------- MIMETYPE & COLOR ----------------------------//

/**
 * @deprecated Make mimetype conversion in server side
 */
const rgxToColorMap = {
  "video/*": "#d9730d",
  "image/*": "#807773",
  "audio/*": "#6940a5",
  "application/vnd.(ms-excel|openxmlformats-officedocument.spreadsheetml.sheet)": "#0f7b6c",
  "application/pdf": "#e03e3e",
  "application/(msword|vnd.openxmlformats-officedocument.wordprocessingml.document)": "#0b6e99"
}

/**
 * @deprecated
 * @param mimeType
 * @returns {*}
 */
export function getColorHexOfMimeType (mimeType) {
  for (const mimeRgx in rgxToColorMap) {
    if ((new RegExp(`\\b${mimeRgx}\\b`, 'g')).test(mimeType)) {
      return rgxToColorMap[mimeRgx]
    }
  }
}

export const mimeTypeToAppType = (_mimetype) => {
  if (/image\/*/.test(_mimetype)) {
    return 'image'
  } else if (/video\/*/.test(_mimetype)) {
    return 'video'
  } else if (/audio\/*/.test(_mimetype)) {
    return 'audio'
  } else if (/application\/vnd.ms-excel*/.test(_mimetype) || _mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || _mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.template') {
    return 'binder'
  } else if (/application\/vnd\.openxmlformats-officedocument.presentationml\.*/.test(_mimetype) || _mimetype === 'application/vnd.ms-powerpoint') {
    return 'presentation'
  } else if (/application\/vnd\.ms-word\.*/.test(_mimetype) || /application\/vnd\.openxmlformats-officedocument\.wordprocessingml\.*/.test(_mimetype || /application\/msword*/.test(_mimetype))) {
    return 'document'
  } else {
    return 'unknow'
  }
}

const fileTypeToColorHexMap = {
  "video": "#d9730d",
  "image": "#807773",
  "audio": "#6940a5",
  "spreadsheet": "#0f7b6c",
  "document-normalized": "#e03e3e",
  "document-writeable": "#0b6e99"
}

/**
 * @param fileType
 * @returns {*}
 */
export function getHexFileColor (fileType) {
  return fileTypeToColorHexMap[fileType] ?? '#9B9A97'
}

//--------------------------- SIZE ----------------------------//

export const formatBytes = (bytes, decimals = 2) => {
  if (!+bytes) return '0 Bytes'

  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
}

//--------------------------- TIME ----------------------------//

export const dateToLapsedTime = (_date) => {
  const seconds = Math.floor((new Date() - new Date(_date)) / 1000)
  let interval = seconds / 31536000
  if (interval > 1) {
    return Math.floor(interval) + ' years'
  }
  interval = seconds / 2592000
  if (interval > 1) {
    return Math.floor(interval) + ' months'
  }
  interval = seconds / 604800
  if (interval > 1) {
    return Math.floor(interval) + ' weeks'
  }
  interval = seconds / 86400
  if (interval > 1) {
    return Math.floor(interval) + ' days'
  }
  interval = seconds / 3600
  if (interval > 1) {
    return Math.floor(interval) + ' hours'
  }
  interval = seconds / 60
  if (interval > 1) {
    return Math.floor(interval) + ' minutes'
  }
  return 'recently'
}
