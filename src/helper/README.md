# Helper Directory

## EN

This directory contain javascript file for inter-components reusable function. He
represents the **utils** directory in other projects configuration.

## FR

Ce repertoire contient des fichier javascript pour des fonctions inter-composants
réutilisable. Représente le répertoire **utils** dans d'autres configurations de
projets.
