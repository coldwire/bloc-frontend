# Icons and Images Rules

## Icons

The project has a model on Figma for frontend contributors. Our designer uses
an icon library called [**Material Design Icon**](#) and available for VueJS framework
integration on [Iconify Website](#).

You will usually use the import like this in VueJS components :

```vue
<script setup>
import { Icon } from '@iconify/vue'
</script>

<template>
  <Icon icon="mdi:star-outline" height="16px" width="16px" />
</template>
```

However, if you use custom icons it will be necessary to create a wrapper component
(VueJS) around that or the **<img\>** tag directly.

## Images

> No content for now
