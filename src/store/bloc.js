// TODO: Rename action 'fillFixtures' by 'loadFixtures'
import { defineStore } from 'pinia'
import blocFixtureJson from '@/fixture/bloc.json'

export const useBlocStore = defineStore('bloc', {
  state: () => ({
    currentBlocCursor: null,
    currentBlocList: [],
    currentBlocMap: {}
  }),
  getters: {
    blocs: (state) => {
      let blocList = []
      state.currentBlocList.forEach(e => {
        blocList.push(state.currentBlocMap[e])
      })
      return blocList
    },
    files: (state) => {
      let fileList = []
      state.currentBlocList.forEach(e => {
        if (!state.currentBlocMap[e].is_directory) {
          fileList.push(state.currentBlocMap[e])
        }
      })
      return fileList
    },
    directories: (state) => {
      let directoryList = []
      state.currentBlocList.forEach(e => {
        if (state.currentBlocMap[e].is_directory) {
          directoryList.push(state.currentBlocMap[e])
        }
      })
      return directoryList
    },
    blocsCount: (state) => {
      return state.currentBlocList.length
    }
  },
  actions: {
    fillFixtures () {
      this.currentBlocCursor = blocFixtureJson.content.current_bloc_cursor
      this.currentBlocMap = blocFixtureJson.content.current_bloc_map
      this.currentBlocList = blocFixtureJson.content.current_bloc_list
    }
  }
})
