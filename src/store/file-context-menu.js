import { defineStore } from 'pinia'

export const useFileContextMenu = defineStore('file-context-menu', {
  state: () => ({
    bid: null,
    eventType: null
  }),
  getters: {},
  actions: {

    /**
     * This function manage display of file context menu with 'button' and 'contextmenu' events in same time
     * @param event the javascript $event
     * @param bid the bloc ID
     */
    show (event, bid) {
      event.preventDefault()
      const fileContextMenu = document.getElementById('file-context-menu')
      if (bid) {
        this.bid = bid
        fileContextMenu.style.top = `${event.clientY}px`
        fileContextMenu.style.left = `${event.clientX}px`
        this.eventType = event.type
        if (event.type === 'contextmenu') {
          fileContextMenu.classList.add('visible')
          document.addEventListener('click', (event) => {
            event.preventDefault()
            const isClickInside = fileContextMenu.contains(event.target)
            if (!isClickInside) {
              fileContextMenu.classList.remove('visible')
            }
          }, { once: true })
        } else {
          fileContextMenu.classList.toggle('visible')
        }
      }
    }
  }
})
