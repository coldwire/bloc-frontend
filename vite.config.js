import { fileURLToPath, URL } from 'url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
        @import "@/asset/scss/font.scss";
        @import "@/asset/scss/variable.scss";
        @import "@/asset/scss/reset.scss";
        @import "@/asset/scss/layout.scss";
        @import "@/asset/scss/button.scss";
        @import "@/asset/scss/notification.scss";
        @import "@/asset/scss/card.scss";
        @import "@/asset/scss/context-menu.scss";`
      }
    }
  }
})
